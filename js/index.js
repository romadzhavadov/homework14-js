const firstTheme = document.querySelector(".main-header");
const btn = document.querySelector(".header-btn");

const theme = {};

btn.addEventListener('click', (e) => {
    // firstTheme.classList.toggle('dark-theme');
    if (firstTheme.style.background === "rgb(251, 240, 244)" || firstTheme.style.background === "") {
        firstTheme.style.background = "#774f5c"
    } 
    else {
        firstTheme.style.background = "#fbf0f4"
    }

    theme['background'] = firstTheme.style.background;
    localStorage.setItem('theme', JSON.stringify(theme))

    })


    if (localStorage.getItem('theme')) {
        const themeObj = JSON.parse(localStorage.getItem('theme'));
        firstTheme.style.background = themeObj['background'];
    }


